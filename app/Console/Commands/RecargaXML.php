<?php



namespace Smartbus\Console\Commands;

use Archiving\SDK\Catalogo;
use Archiving\SDK\Documento;
use Archiving\SDK\Client;


use Illuminate\Console\Command;
use Smartbus\Library\Bitacora;

class RecargaXML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Smartbus:recargaxml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 
    'Genera grupo de archivos xml de transacciones de recarga junto con el archivo de control';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      

      $id_colecta = 0;

      $fecha_colecta = date("Y-m-d h:i:s");

      $fecha_inicial = date("Y-m-d") . " 00:00:00";

      $fecha_final = date("Y-m-d") . "23:59:59";

      $fecha_custom = "2024-04-08 00:00:00";

      $EUR = "SMARTBUS01";

      $id_ruta = 0;

      $id_bpd = 4331;

      $proveedor_tecnologico = 0;

      $registro_fimpe = null;

      $doc_resp = '';

      $validaciones = null;

      $numero_total_recargas = 0;

      $monto_total_recargas = 0;


      $cliente = new Client([
          'base_url' => 'http://kepler.klayware.com/',
          'email' => 'desarrollo@stx.com.mx',
          'apikey' => 'IiCrl0OGCmkLXRbDC32wE',
          'scope' => 'smartbus'
        ]);

        
        $rutas = Catalogo::consultar("rutas",$cliente);


        foreach ($rutas as $ruta) {

          $numero_total_recargas = 0;

          $monto_total_recargas = 0;

          //Bitacora::infoLog($ruta["payload"]["label"]); 

          dump('crear', $ruta["payload"]["label"] . "  " .  $ruta["payload"]["external_id"]);


         // $rutaID = str_pad($ruta["payload"]["external_id"], 4, '0', STR_PAD_LEFT);


          $recargas = Documento::consultar('recargas', $cliente,
          ["filter"=>["where"=>[["field"=>"payload->encabezado->fecha_hora","operator"=>">=","value"=>$fecha_inicial],
          ["field"=>"payload->encabezado->fecha_hora","operator"=>"<=","value"=>$fecha_final],
          ["field"=>"payload->encabezado->id_ruta","operator"=>"=","value"=>(int)$ruta["payload"]["external_id"]]]]]);


         // dump('ruta id',(int)$ruta["payload"]["external_id"]);


          //Revisar si ya existe un registro de fimpe 
          $registro_fimpe = Documento::consultar('registros_fimpe', $cliente,
          ["filter"=>["where"=>[["field"=>"payload->fecha","operator"=>"=","value"=>date("Y-m-d")],
          ["field"=>"payload->ruta","operator"=>"=","value"=>$ruta["payload"]["external_id"]],["field"=>"payload->tipo","operator"=>"=","value"=>"Recarga"]]]]);

          $registro_fimpe = json_decode($registro_fimpe,true);

          $recargas = json_decode($recargas,true);
          dump('recargas', $recargas);

          //$recargas = null;
          $registro_fimpe = null;
         
          if($recargas != null){



                if($registro_fimpe == null)
                  {
                    //$fechacorte = date("Y-m-d h:i:s");
                    $numero_total_recargas = count($recargas);

                    foreach ($recargas as $recarga) {
                       $monto_total_recargas +=  number_format((float)$recarga['payload']['encabezado']['monto'], 2, '.', '');
                    }


                    $nuevo_registro_fimpe = ['tipo'=>'Recarga','fecha'=> date("Y-m-d"), 'ruta'=> $ruta["payload"]["external_id"]];

                    $doc_registro_fimpe = new Documento('registros_fimpe',  $cliente);

                    $doc_resp =  $doc_registro_fimpe->crear('a',$nuevo_registro_fimpe);

                    $id_colecta = $doc_resp['folio'];

                    dump('crear parte 1', $doc_resp);


                    //$validaciones = json_decode($validaciones,true);



                    $xmlfile = $this->generar_recargas_xml($recargas);

                    $xmlname = "R_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . "_" . $ruta["payload"]["external_id"] . ".DAT";

                    $path = "/var/www/apps/Smartbus/htarocc_v1.3/cifrado/input/" . $xmlname;

                    file_put_contents($path ,$xmlfile);




                    

                   $nuevo_registro_fimpe = ['archivo'=> $xmlname,'tipo'=>'Recarga','fecha'=> date("Y-m-d"), 'ruta'=>$ruta["payload"]["external_id"]];

                   $doc_resp =  $doc_registro_fimpe->actualizar($nuevo_registro_fimpe);

                   


                    dump('crear parte 2', $doc_resp);


                  // Archivo Cifras de Control

                  $cc = xmlwriter_open_memory();
                  xmlwriter_set_indent($cc, 1);
                  $res = xmlwriter_set_indent_string($cc, ' ');
                  xmlwriter_start_document($cc, '1.0', 'UTF-8');
          
                  xmlwriter_start_element($cc, 'archivo');
          
                  xmlwriter_start_attribute($cc, 'tipo');
                  xmlwriter_text($cc, 'ventasrecarga');
                  xmlwriter_end_attribute($cc);

                  xmlwriter_start_element($cc, 'versionlayout');
                  xmlwriter_text($cc, 1.0);
                  xmlwriter_end_element($cc); 

                  xmlwriter_start_element($cc, 'cifrascontrol');

                  // Identificador de colecta
                  xmlwriter_start_element($cc, 'c1');
                  xmlwriter_text($cc, 1.0);
                  xmlwriter_end_element($cc); 
                  // Fecha y Hora de Colecta 
                  xmlwriter_start_element($cc, 'c2');
                  xmlwriter_text($cc, $fecha_colecta);
                  xmlwriter_end_element($cc); 
                  // Fecha y Hora Inicial de Corte
                  xmlwriter_start_element($cc, 'c3');
                  xmlwriter_text($cc, $fecha_inicial);
                  xmlwriter_end_element($cc); 
                  // Fecha y Hora Final de Corte
                  xmlwriter_start_element($cc, 'c4');
                  xmlwriter_text($cc, $fecha_final);
                  xmlwriter_end_element($cc); 
                  // Identificador del operador
                  xmlwriter_start_element($cc, 'c5');
                  xmlwriter_text($cc, $EUR);
                  xmlwriter_end_element($cc); 
                  // Identificador del corredor/ruta
                  xmlwriter_start_element($cc, 'c6');
                  xmlwriter_text($cc, $ruta["payload"]["external_id"]);
                  xmlwriter_end_element($cc); 
                  // Numero Total de Registros
                  xmlwriter_start_element($cc, 'c7');
                  xmlwriter_text($cc,count($recargas));
                  xmlwriter_end_element($cc); 
                  // Monto Total de Registros (Sin BPDs)
                  xmlwriter_start_element($cc, 'c8');
                  xmlwriter_text($cc,$monto_total_recargas);
                  xmlwriter_end_element($cc); 
                  // Numero Total de Ventas de Tarjetas
                  xmlwriter_start_element($cc, 'c9');
                  xmlwriter_text($cc,0);
                  xmlwriter_end_element($cc);
                  // Monto Total por las Ventas
                  xmlwriter_start_element($cc, 'c10');
                  xmlwriter_text($cc,0);
                  xmlwriter_end_element($cc);
                  // Numero Total de Recargas
                  xmlwriter_start_element($cc, 'c11');
                  xmlwriter_text($cc,0);
                  xmlwriter_end_element($cc);
                  // Monto Total por las Recargas (Sin BPDs)
                  xmlwriter_start_element($cc, 'c12');
                  xmlwriter_text($cc,0);
                  xmlwriter_end_element($cc);
                  // Total de viajes (BPDs)
                  xmlwriter_start_element($cc, 'c13');
                  xmlwriter_text($cc,0);
                  xmlwriter_end_element($cc);
                  // Identificador del Proveedor Tecnologico
                  xmlwriter_start_element($cc, 'c14');
                  xmlwriter_text($cc,$EUR);
                  xmlwriter_end_element($cc); 
               
                  xmlwriter_end_element($cc); 

                  xmlwriter_start_element($cc, 'llaveAES');
                  xmlwriter_text($cc, 0);
                  xmlwriter_end_element($cc); 

                  
                  xmlwriter_end_element($cc); 

                  xmlwriter_end_document($cc);
                  
                  $ccfile = xmlwriter_output_memory($cc);

                  $ccname = "R_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . "_" . $ruta["payload"]["external_id"] . ".CC";


                  $path = "/var/www/apps/Smartbus/htarocc_v1.3/upload/" . $ccname;

                  file_put_contents($path ,$ccfile);
                  }
                  else {
                    dump("Fimpe","Existe Registro Fimpe");
                  }

                }

                else {
                  dump("Recargas","No Existen Documentos");
                  
                }
          
          }




        




    }




     function generar_recargas_xml($transacciones)
     {

        $xw = xmlwriter_open_memory();
        xmlwriter_set_indent($xw, 1);
        $res = xmlwriter_set_indent_string($xw, ' ');
        xmlwriter_start_document($xw, '1.0', 'UTF-8');

        xmlwriter_start_element($xw, 'archivo');

        xmlwriter_start_attribute($xw, 'tipo');
        xmlwriter_text($xw, 'ventarecargas');
        xmlwriter_end_attribute($xw);


        foreach ($transacciones as $transaccion) {

            //Bitacora::infoLog($transaccion['payload']['encabezado']);
            xmlwriter_start_element($xw, 'recarga');
          
            // Folio de transaccion
            xmlwriter_start_element($xw, 'c1');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['folio_transaccion']);
            xmlwriter_end_element($xw); 

            // Fecha y hora de transaccion 
            xmlwriter_start_element($xw, 'c2');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['fecha_hora']);
            xmlwriter_end_element($xw); 

            // Identificador de tarjeta
            xmlwriter_start_element($xw, 'c3');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['uid']);
            xmlwriter_end_element($xw); 

            // Producto
            xmlwriter_start_element($xw, 'c4');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['id_producto']);
            xmlwriter_end_element($xw); 

            // Monto de la transaccion
            xmlwriter_start_element($xw, 'c5');
            xmlwriter_text($xw, number_format((float)$transaccion['payload']['encabezado']['monto'], 2, '.', '') );
            xmlwriter_end_element($xw); 

            // Inicio de vigencia del producto *Opcional *Obligatorio para BPD
            xmlwriter_start_element($xw, 'c6');
            xmlwriter_end_element($xw); 

             // Fin de vigencia del producto *Opcional *Obligatorio para BPD
             xmlwriter_start_element($xw, 'c7');
             xmlwriter_end_element($xw); 

            // Inicio de validez durante el dia  *Opcional *Obligatorio para BPD
            xmlwriter_start_element($xw, 'c8');
            xmlwriter_end_element($xw); 

            // Fin de validez durante el dia  *Opcional *Obligatorio para BPD
            xmlwriter_start_element($xw, 'c9');
            xmlwriter_end_element($xw); 

            // Saldo inicial del producto
            xmlwriter_start_element($xw, 'c10');
            xmlwriter_text($xw, number_format((float)$transaccion['payload']['encabezado']['saldo_inicial'], 2, '.', ''));
            xmlwriter_end_element($xw); 

            // Saldo final del producto
            xmlwriter_start_element($xw, 'c11');
            xmlwriter_text($xw, number_format((float)$transaccion['payload']['encabezado']['saldo_final'], 2, '.', ''));
            xmlwriter_end_element($xw);

            // Identificador del dispositivo 
            xmlwriter_start_element($xw, 'c12');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['id_dispositivo']);
            xmlwriter_end_element($xw);

            // Identificador de SAM
            xmlwriter_start_element($xw, 'c13');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['id_sam']);
            xmlwriter_end_element($xw);

            // Numero de transaccion en SAM
            xmlwriter_start_element($xw, 'c14');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['consecutivo_sam']);
            xmlwriter_end_element($xw);

            // Numero de transaccion en la tarjeta 
            xmlwriter_start_element($xw, 'c15');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['consecutivo_aplicacion']);
            xmlwriter_end_element($xw);

            // Latitud
            xmlwriter_start_element($xw, 'c16');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['latitud']);
            xmlwriter_end_element($xw);

            // Longitud
            xmlwriter_start_element($xw, 'c17');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['longitud']);
            xmlwriter_end_element($xw);

            // Tipo Recarga
            xmlwriter_start_element($xw, 'c18');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['tipo_recarga']);
            xmlwriter_end_element($xw);

            // Parametros de firma 
            xmlwriter_start_element($xw, 'c19');
            //xmlwriter_text($xw, 'parametros de firma');
            xmlwriter_end_element($xw);

            // Firma de la transaccion 
            xmlwriter_start_element($xw, 'c20');
           // xmlwriter_text($xw, 'firma');
            xmlwriter_end_element($xw);

            // Identificador de la estacion o vehiculo 
            xmlwriter_start_element($xw, 'c21');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['id_ruta']);
            xmlwriter_end_element($xw);

            // Identificador de tarjeta de destino en caso de transferencia de saldo *Opcional
           // xmlwriter_start_element($xw, 'c22')
           //mlwriter_end_element($xw);

           // xmlwriter_end_element($xw); 


        }

        xmlwriter_end_document($xw);


        $xmlfile = xmlwriter_output_memory($xw);

        return $xmlfile;

        //$xmlname = "R_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . "_" . $id_ruta;

       // $path = "/var/www/apps/Smartbus/storage/" . $xmlname . ".DAT";

       // file_put_contents($path ,$xmlfile);
    
     }



   /*   function generar_cifras_control($transacciones,$id_colecta,$fecha_colecta,$fecha_inicial,$fecha_final,$EUR,$id_ruta,$id_bpd,$proveedor_tecnologico){


      $monto_total = 0;
      $numero_total = 0;
      $viajes_bpd = 0;

      foreach ($transacciones as $transaccion) {

            if($transaccion['payload']['encabezado']['id_producto'] == $id_bpd)
            {

              $viajes_bpd++;
            }

        
            $monto_total += $transaccion['payload']['encabezado']['monto'];
            $numero_total++;
        }





      $xw = xmlwriter_open_memory();
      xmlwriter_set_indent($xw, 1);
      $res = xmlwriter_set_indent_string($xw, ' ');
    
      xmlwriter_start_element($xw, 'archivo');

      xmlwriter_start_attribute($xw, 'tipo');
      xmlwriter_text($xw, 'recarga');
      xmlwriter_end_attribute($xw);

      xmlwriter_start_element($xw, 'versionlayout');
      xmlwriter_text($xw, '1.0');
      xmlwriter_end_element($xw);

      xmlwriter_start_element($xw, 'cifrascontrol');

      // Identificador de colecta
      xmlwriter_start_element($xw, 'c1');
      xmlwriter_text($xw, $id_colecta);
      xmlwriter_end_element($xw); 

      // Fecha y Hora de colecta
      xmlwriter_start_element($xw, 'c2');
      xmlwriter_text($xw,$fecha_colecta);
      xmlwriter_end_element($xw); 

      // Fecha y hora inicial de corte
      xmlwriter_start_element($xw, 'c3');
      xmlwriter_text($xw,$fecha_inicial);
      xmlwriter_end_element($xw); 

      // Fecha y hora final de corte
      xmlwriter_start_element($xw, 'c4');
      xmlwriter_text($xw, $fecha_final);
      xmlwriter_end_element($xw); 

      // Identificador del operador
      xmlwriter_start_element($xw, 'c5');
      xmlwriter_text($xw, $EUR);
      xmlwriter_end_element($xw); 

      //identificador del corredor/ruta
      xmlwriter_start_element($xw, 'c6');
      xmlwriter_text($xw, $id_ruta);
      xmlwriter_end_element($xw); 

      // Numero total de registros
      xmlwriter_start_element($xw, 'c7');
      xmlwriter_text($xw, $numero_total);
      xmlwriter_end_element($xw);

      // Monto total de registros
      xmlwriter_start_element($xw, 'c8');
      xmlwriter_text($xw, number_format((float)$monto_total 2, '.', ''));
      xmlwriter_end_element($xw);

      // Identificador del proveedor tecnologico 
      xmlwriter_start_element($xw, 'c9');
      xmlwriter_text($xw ,$proveedor_tecnologico);
      xmlwriter_end_element($xw);

      // Total de viajes(BPD's)
      xmlwriter_start_element($xw, 'c10');
      xmlwriter_text($xw, $viajes_bpd);
      xmlwriter_end_element($xw);

      xmlwriter_end_element($xw);

      xmlwriter_end_document($xw);

      $xmlfile = xmlwriter_output_memory($xw);

      $xmlname = "R_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . "_" . $id_ruta;

      $path = "/var/www/apps/Smartbus/storage/" . $xmlname . ".CC";
      
      file_put_contents($path ,$xmlfile);

     }*/
}


