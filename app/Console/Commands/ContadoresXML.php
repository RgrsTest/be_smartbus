<?php



namespace Smartbus\Console\Commands;

use Archiving\SDK\Catalogo;
use Archiving\SDK\Documento;
use Archiving\SDK\Client;


use Illuminate\Console\Command;
use Smartbus\Library\Bitacora;

class ContadoresXML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Smartbus:contadoresxml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 
    'Genera grupo de archivos xml de transacciones en efectivo junto con el archivo de control';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      

      $id_colecta = 1;

      $fecha_colecta = date("Y-m-d h:i:s");

      $fecha_inicial = date("Y-m-d h:i:s");

      $fecha_final = date("Y-m-d h:i:s");

      $EUR = "SMARTBUS01";

      $id_ruta = 0;

      $id_bpd = 4331;

      $total_subidas = 0;

      $total_bajadas = 0;

      $proveedor_tecnologico = 0;


      $cliente = new Client([
          'base_url' => 'http://kepler.klayware.com/',
          'email' => 'desarrollo@stx.com.mx',
          'apikey' => 'IiCrl0OGCmkLXRbDC32wE',
          'scope' => 'smartbus'
        ]);

        $rutas = Catalogo::consultar('rutas', $cliente);
    
        $vehiculos = Catalogo::consultar('vehiculos', $cliente);

       
       


        foreach ($rutas as $ruta) {

          $total_subidas = 0;

          $total_bajadas = 0;


          $registro_fimpe = Documento::consultar('registros_fimpe', $cliente,
          ["filter"=>["where"=>[["field"=>"payload->fecha","operator"=>"=","value"=>date("Y-m-d")],
          ["field"=>"payload->ruta","operator"=>"=","value"=>$ruta["payload"]["external_id"]],["field"=>"payload->tipo","operator"=>"=","value"=>"Contador"]]]]);

          $registro_fimpe = json_decode($registro_fimpe,true);


          dump('Registro Fimpe',  $registro_fimpe);

          $registro_fimpe = null;
          if($registro_fimpe == null)
          {


                  $nuevo_registro_fimpe = ['tipo'=>'Contador','fecha'=> date("Y-m-d"), 'ruta'=> $ruta["payload"]["external_id"]];

                  $doc_registro_fimpe = new Documento('registros_fimpe',  $cliente);

                  $doc_resp =  $doc_registro_fimpe->crear('a',$nuevo_registro_fimpe);

                  $id_colecta = $doc_resp['folio'];

                  dump('crear parte 1', $doc_resp);



                 





                  // Archivo de transaccion del contador de la ruta
                  $xw = xmlwriter_open_memory();
                  xmlwriter_set_indent($xw, 1);
                  $res = xmlwriter_set_indent_string($xw, ' ');
                  xmlwriter_start_document($xw, '1.0', 'UTF-8');
          
                  xmlwriter_start_element($xw, 'archivo');
          
                  xmlwriter_start_attribute($xw, 'tipo');
                  xmlwriter_text($xw, 'contador');
                  xmlwriter_end_attribute($xw);


                  foreach ($vehiculos as $vehiculo) {

                    dump('VEHICULO',$vehiculo);

                    dump('VEHICULO',  isset($vehiculo['payload']['location']['subidas']));


                    dump('VEHICULO',  isset($vehiculo['payload']['location']['bajadas']));


                    if(isset($vehiculo['payload']['location']['subidas']) && isset($vehiculo['payload']['location']['bajadas'])){


                  

                          if($vehiculo['payload']['id_ruta'] == (int)$ruta['payload']['external_id'])
                          {

                            $total_subidas +=  $vehiculo['payload']['location']['subidas'];
                            $total_bajadas +=  $vehiculo['payload']['location']['bajadas'];

                            xmlwriter_start_element($xw, 'contador');

                            // Identificador de  vehiculo 
                            xmlwriter_start_element($xw, 'c1');
                            xmlwriter_text($xw, $vehiculo['payload']['label']);
                            xmlwriter_end_element($xw); 

                            // Fecha
                            xmlwriter_start_element($xw, 'c2');
                            xmlwriter_text($xw, date("Y-m-d"));
                            xmlwriter_end_element($xw);            

                            // Numero de subidas
                            xmlwriter_start_element($xw, 'c3');
                            xmlwriter_text($xw, $vehiculo['payload']['location']['subidas']);
                            xmlwriter_end_element($xw); 

                            // Numero de bajadas
                            xmlwriter_start_element($xw, 'c4');
                            xmlwriter_text($xw, $vehiculo['payload']['location']['bajadas']);
                            xmlwriter_end_element($xw); 

                            xmlwriter_end_element($xw); 

                          }

                        }

                    }
                    xmlwriter_end_element($xw); 
                    xmlwriter_end_document($xw);


                    $xmlfile = xmlwriter_output_memory($xw);

                    $xmlname = "C_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . "_" . $ruta["payload"]["external_id"] . ".DAT";

                    $path = "/var/www/apps/Smartbus/htarocc_v1.3/cifrado/input/" . $xmlname;

                    file_put_contents($path ,$xmlfile);

                    $nuevo_registro_fimpe = ['archivo'=> $xmlname,'tipo'=>'Contador','fecha'=> date("Y-m-d"), 'ruta'=>$ruta["payload"]["external_id"]];

                    $doc_resp =  $doc_registro_fimpe->actualizar($nuevo_registro_fimpe);

                    dump('CONTADORES', $xmlfile);


                     // Archivo de cifras de control del contador  de la ruta
                      $cc = xmlwriter_open_memory();
                      xmlwriter_set_indent($cc, 1);
                      $res = xmlwriter_set_indent_string($cc, ' ');
                      xmlwriter_start_document($cc, '1.0', 'UTF-8');
              
                      xmlwriter_start_element($cc, 'archivo');
              
                      xmlwriter_start_attribute($cc, 'tipo');
                      xmlwriter_text($cc, 'contador');
                      xmlwriter_end_attribute($cc);

                      xmlwriter_start_element($cc, 'versionlayout');
                      xmlwriter_text($cc, 1.0);
                      xmlwriter_end_element($cc); 

                      xmlwriter_start_element($cc, 'cifrascontrol');

                      // Identificador de colecta
                      xmlwriter_start_element($cc, 'c1');
                      xmlwriter_text($cc, 1.0);
                      xmlwriter_end_element($cc); 
                      // Fecha y Hora de Colecta 
                      xmlwriter_start_element($cc, 'c2');
                      xmlwriter_text($cc, $fecha_colecta);
                      xmlwriter_end_element($cc); 
                      // Fecha y Hora Inicial de Corte
                      xmlwriter_start_element($cc, 'c3');
                      xmlwriter_text($cc, $fecha_inicial);
                      xmlwriter_end_element($cc); 
                      // Fecha y Hora Final de Corte
                      xmlwriter_start_element($cc, 'c4');
                      xmlwriter_text($cc, $fecha_final);
                      xmlwriter_end_element($cc); 
                      // Identificador del operador
                      xmlwriter_start_element($cc, 'c5');
                      xmlwriter_text($cc, $EUR);
                      xmlwriter_end_element($cc); 
                      // Identificador del corredor/ruta
                      xmlwriter_start_element($cc, 'c6');
                      xmlwriter_text($cc, $ruta["payload"]["external_id"]);
                      xmlwriter_end_element($cc); 
                      // Numero Total de Registros
                      xmlwriter_start_element($cc, 'c7');
                      xmlwriter_text($cc,count($vehiculos));
                      xmlwriter_end_element($cc); 
                      // Total de Subidas 
                      xmlwriter_start_element($cc, 'c8');
                      xmlwriter_text($cc,$total_subidas);
                      xmlwriter_end_element($cc); 
                      // Total de bajadas
                      xmlwriter_start_element($cc, 'c9');
                      xmlwriter_text($cc, $total_bajadas);
                      xmlwriter_end_element($cc); 
                      // Identificador del Proveedor Tecnologico
                      xmlwriter_start_element($cc, 'c10');
                      xmlwriter_text($cc,$EUR);
                      xmlwriter_end_element($cc); 

                      xmlwriter_end_element($cc); 

                      xmlwriter_start_element($cc, 'llaveAES');
                      xmlwriter_text($cc, 0);
                      xmlwriter_end_element($cc); 

                      xmlwriter_end_element($cc); 

                      xmlwriter_end_document($cc);
                      
                      $ccfile = xmlwriter_output_memory($cc);

                      $ccname = "C_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . "_" . $ruta["payload"]["external_id"] . ".CC";


                      $path = "/var/www/apps/Smartbus/htarocc_v1.3/upload/" . $ccname;

                      file_put_contents($path ,$ccfile);
       

                    dump('CIFRAS DE CONTROL', $ccfile);
            }



        }




    }


/*
    public function generar_contadores_xml($transacciones,$id_colecta,$fecha_colecta,$EUR,$id_ruta)
     {

        $xw = xmlwriter_open_memory();
        xmlwriter_set_indent($xw, 1);
        $res = xmlwriter_set_indent_string($xw, ' ');
        xmlwriter_start_document($xw, '1.0', 'UTF-8');

        xmlwriter_start_element($xw, 'archivo');

        xmlwriter_start_attribute($xw, 'tipo');
        xmlwriter_text($xw, 'contador');
        xmlwriter_end_attribute($xw);


        foreach ($transacciones as $transaccion) {

            xmlwriter_start_element($xw, 'contador');
          
            // Identificador de la estacion o vehiculo 
            xmlwriter_start_element($xw, 'c1');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['_relaciones']['vehiculo']['label']);
            xmlwriter_end_element($xw); 

            // Fecha 
            xmlwriter_start_element($xw, 'c2');
            xmlwriter_text($xw, strtok($transaccion['payload']['encabezado']['fecha_hora'], ' '));
            xmlwriter_end_element($xw); 

            // Importe
            xmlwriter_start_element($xw, 'c3');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['uid']);
            xmlwriter_end_element($xw); 

            // Numero de Pasajeros
            xmlwriter_start_element($xw, 'c4');
            xmlwriter_text($xw, $transaccion['payload']['encabezado']['id_producto']);
            xmlwriter_end_element($xw); 


            xmlwriter_end_element($xw); 


        }

        xmlwriter_end_document($xw);


        $xmlfile = xmlwriter_output_memory($xw);

        $xmlname = "R_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . "_" . $id_ruta;

        $path = "/var/www/apps/Smartbus/storage/" . $xmlname . ".DAT";

        file_put_contents($path ,$xmlfile);
    
     }*/


/*
     public function generar_cifras_control($transacciones,$id_colecta,$fecha_colecta,$fecha_inicial,$fecha_final,$EUR,$id_ruta,$id_bpd,$proveedor_tecnologico){


      $monto_total = 0;
      $numero_total = 0;
      $viajes_bpd = 0;
      $pasajeros = 0;

      foreach ($transacciones as $transaccion) {

            if($transaccion['payload']['encabezado']['id_producto'] == $id_bpd)
            {

              $viajes_bpd++;

            }



        
            $monto_total += $transaccion['payload']['encabezado']['monto'];
            $numero_total++;
        }





      $xw = xmlwriter_open_memory();
      xmlwriter_set_indent($xw, 1);
      $res = xmlwriter_set_indent_string($xw, ' ');
    
      xmlwriter_start_element($xw, 'archivo');

      xmlwriter_start_attribute($xw, 'tipo');
      xmlwriter_text($xw, 'debito');
      xmlwriter_end_attribute($xw);

      xmlwriter_start_element($xw, 'versionlayout');
      xmlwriter_text($xw, '1.0');
      xmlwriter_end_element($xw);

      xmlwriter_start_element($xw, 'cifrascontrol');

      // Identificador de colecta
      xmlwriter_start_element($xw, 'c1');
      xmlwriter_text($xw, $id_colecta);
      xmlwriter_end_element($xw); 

      // Fecha y Hora de colecta
      xmlwriter_start_element($xw, 'c2');
      xmlwriter_text($xw,$fecha_colecta);
      xmlwriter_end_element($xw); 

      // Fecha y hora inicial de corte
      xmlwriter_start_element($xw, 'c3');
      xmlwriter_text($xw,$fecha_inicial);
      xmlwriter_end_element($xw); 

      // Fecha y hora final de corte
      xmlwriter_start_element($xw, 'c4');
      xmlwriter_text($xw, $fecha_final);
      xmlwriter_end_element($xw); 

      // Identificador del operador
      xmlwriter_start_element($xw, 'c5');
      xmlwriter_text($xw, $EUR);
      xmlwriter_end_element($xw); 

      //identificador del corredor/ruta
      xmlwriter_start_element($xw, 'c6');
      xmlwriter_text($xw, $id_ruta);
      xmlwriter_end_element($xw); 

      // Numero total de registros
      xmlwriter_start_element($xw, 'c7');
      xmlwriter_text($xw, $numero_total);
      xmlwriter_end_element($xw);

      // Monto total de registros
      xmlwriter_start_element($xw, 'c8');
     // xmlwriter_text($xw, number_format((float)$monto_total 2, '.', ''));
      xmlwriter_end_element($xw);

      // Pasajeros
      xmlwriter_start_element($xw, 'c9');
      xmlwriter_text($xw ,$proveedor_tecnologico);
      xmlwriter_end_element($xw);

      // Identificador del proveedor tecnologico 
      xmlwriter_start_element($xw, 'c9');
      xmlwriter_text($xw ,$proveedor_tecnologico);
      xmlwriter_end_element($xw);

     

      xmlwriter_end_element($xw);

      xmlwriter_end_document($xw);

      $xmlfile = xmlwriter_output_memory($xw);

      $xmlname = "R_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . "_" . $id_ruta;

      $path = "/var/www/apps/Smartbus/storage/" . $xmlname . ".CC";
      
      file_put_contents($path ,$xmlfile);

     }*/
}


