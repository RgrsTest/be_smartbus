<?php

namespace Smartbus\Console\Commands;


use Archiving\SDK\Catalogo;
use Archiving\SDK\Documento;
use Archiving\SDK\Client;
use Klayware\Exceptions\KlayException;
use Illuminate\Console\Command;
use Smartbus\Library\Bitacora;

class Prueba extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Smartbus:prueba';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    //   config(['database.default' => 'Smartbus']);

      # ...

     // file_put_contents("test.txt","Hello World. Testing!");
      // console.log("testing task scheduling"); 
      // Bitacora::infoLog("testing task scheduling");
      //Log::channel("Smartbus")->info("testing task scheduling");
      //$vehiculos = (new Catalogo('vehiculos'))->all();
      //$vehiculos = json_decode($vehiculos, true);
    //  Bitacora::infoLog($vehiculos);
    // $test2 = $test->dashboard();


    $cliente = new Client([
      'base_url' => 'http://kepler.klayware.com/',
      'email' => 'desarrollo@stx.com.mx',
      'apikey' => 'IiCrl0OGCmkLXRbDC32wE',
      'scope' => 'smartbus'
    ]);

    $validaciones = Documento::consultar('validaciones', $cliente,
    ["filter"=>["where"=>[["field"=>"payload->encabezado->fecha_hora","operator"=>">=","value"=>"2023-09-01T20:02:20.000Z"],
    ["field"=>"payload->encabezado->fecha_hora","operator"=>"<=","value"=>"2023-12-29T21:02:20.004Z"]]]]);
    
    $validaciones = json_decode($validaciones,true);

    Bitacora::infoLog($validaciones);
    }
}
