<?php



namespace Smartbus\Console\Commands;

use Archiving\SDK\Catalogo;
use Archiving\SDK\Documento;
use Archiving\SDK\Client;


use Illuminate\Console\Command;
use Smartbus\Library\Bitacora;

class RecargaRemotaML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Smartbus:recargaremotaxml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 
    'Genera grupo de archivos xml de transacciones de recarga junto con el archivo de control';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      

        $id_colecta = 1;

        $fecha_colecta = date("Y-m-d h:i:s");

        $fecha_inicial = date("Y-m-d h:i:s");

        $fecha_final = date("Y-m-d h:i:s");

        $EUR = "SMARTBUS";

        $id_ruta = 0;

        $id_bpd = 4331;

        $proveedor_tecnologico = 0;

        $enable_save = 0;

        $numero_total_solicitudes = 0;

        $monto_total_solicitudes = 0;

        $numero_total_confirmaciones = 0;

        $monto_total_confirmaciones = 0;


        $cliente = new Client([
            'base_url' => 'http://kepler.klayware.com/',
            'email' => 'desarrollo@stx.com.mx',
            'apikey' => 'IiCrl0OGCmkLXRbDC32wE',
            'scope' => 'smartbus'
          ]);
      
          $recargasremotas = Documento::consultar('lapr', $cliente);

          // $numero_total_solicitudes = count($recargasremotas);

          
          //Revisar si ya existe un registro de fimpe 
          $registro_fimpe = Documento::consultar('registros_fimpe', $cliente,
          ["filter"=>["where"=>[["field"=>"payload->fecha","operator"=>"=","value"=>date("Y-m-d")],
          ["field"=>"payload->tipo","operator"=>"=","value"=>"LAPR"]]]]);

          $registro_fimpe = json_decode($registro_fimpe,true);


          $xw = xmlwriter_open_memory();
          xmlwriter_set_indent($xw, 1);
          $res = xmlwriter_set_indent_string($xw, ' ');
          xmlwriter_start_document($xw, '1.0', 'UTF-8');
  
          xmlwriter_start_element($xw, 'archivo');
  
          xmlwriter_start_attribute($xw, 'tipo');
          xmlwriter_text($xw, 'lapr');
          xmlwriter_end_attribute($xw);
 
          xmlwriter_start_element($xw, 'solicitudes');



          $fechacorte = date("Y-m-d h:i:s");
          
         // $recargasremotas = json_decode($recargasremotas,true);
         $registro_fimpe = null;
          if($registro_fimpe == null)
          {

            $nuevo_registro_fimpe = ['tipo'=>'LAPR','fecha'=> date("Y-m-d")];

            $doc_registro_fimpe = new Documento('registros_fimpe',  $cliente);

            $doc_resp =  $doc_registro_fimpe->crear('a',$nuevo_registro_fimpe);

            $id_colecta = $doc_resp['folio'];

          
              foreach($recargasremotas as $lapr){


                if($lapr['payload']['sam']){

                  $enable_save = 1;

                  $numero_total_confirmaciones += 1;

                  $numero_total_registros += 1;

                  $monto_total_confirmaciones += $lapr['payload']['monto'];

                  xmlwriter_start_element($xw, 'confirmaciones');

                  // Identificador de tarjeta
                  xmlwriter_start_element($xw, 'c1');
                  xmlwriter_text($xw, $lapr['payload']['uid']);
                  xmlwriter_end_element($xw); 

                  // Fecha y hora de registro
                  xmlwriter_start_element($xw, 'c2');
                  xmlwriter_text($xw, $lapr['payload']['fecha_hora_registro']);
                  xmlwriter_end_element($xw); 

                  // Numero Consecutivo de Accion
                  xmlwriter_start_element($xw, 'c3');
                  xmlwriter_text($xw, $lapr['payload']['numero_accion_aplicada']);
                  xmlwriter_end_element($xw); 

                  // Monto de la recarga
                  xmlwriter_start_element($xw, 'c4');
                  xmlwriter_text($xw, $lapr['payload']['monto']);
                  xmlwriter_end_element($xw); 

                  // Producto
                  xmlwriter_start_element($xw, 'c5');
                  xmlwriter_text($xw, $lapr['payload']['idProducto']);
                  xmlwriter_end_element($xw); 

                  // Tipo de Recarga
                  xmlwriter_start_element($xw, 'c6');
                  xmlwriter_text($xw, 001);
                  xmlwriter_end_element($xw); 

                  // Identificador de SAM
                  xmlwriter_start_element($xw, 'c7');
                  xmlwriter_text($xw, $lapr['payload']['codigo_accion']);
                  xmlwriter_end_element($xw); 

                  xmlwriter_end_element($xw); 

                  xmlwriter_end_element($xw); 


                }

              }

        

              xmlwriter_end_element($xw); 

              xmlwriter_end_document($xw);


              $xmlfile = xmlwriter_output_memory($xw); 

              dump('archivo xml', $xmlfile);


              if($enable_save){

                $xmlname = "LR_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR  . ".DAT";

                $path = "/var/www/apps/Smartbus/htarocc_v1.3/cifrado/input/". $xmlname;
  
                file_put_contents($path ,$xmlfile);
  
                $nuevo_registro_fimpe = ['archivo'=> $xmlname,'tipo'=>'LAPR','fecha'=> date("Y-m-d")];
  
                $doc_resp =  $doc_registro_fimpe->actualizar($nuevo_registro_fimpe);

                // Archivo Cifras de Control

                $cc = xmlwriter_open_memory();
                xmlwriter_set_indent($cc, 1);
                $res = xmlwriter_set_indent_string($cc, ' ');
                xmlwriter_start_document($cc, '1.0', 'UTF-8');
        
                xmlwriter_start_element($cc, 'archivo');
        
                xmlwriter_start_attribute($cc, 'tipo');
                xmlwriter_text($cc, 'lapr');
                xmlwriter_end_attribute($cc);

                xmlwriter_start_element($cc, 'versionlayout');
                xmlwriter_text($cc, 1.0);
                xmlwriter_end_element($cc); 

                xmlwriter_start_element($cc, 'cifrascontrol');

                // Identificador de colecta
                xmlwriter_start_element($cc, 'c1');
                xmlwriter_text($cc, 1.0);
                xmlwriter_end_element($cc); 
                // Fecha y Hora de Generacion
                xmlwriter_start_element($cc, 'c2');
                xmlwriter_text($cc, $fecha_colecta);
                xmlwriter_end_element($cc); 
                // Numero Total de Registros
                xmlwriter_start_element($cc, 'c3');
                xmlwriter_text($cc,$numero_total_registros);
                xmlwriter_end_element($cc); +
                // Monto Total de Registros
                xmlwriter_start_element($cc, 'c4');
                xmlwriter_text($cc,$numero_total_registros);
                xmlwriter_end_element($cc); 
                // Numero Total de Solicitudes
                xmlwriter_start_element($cc, 'c5');
                xmlwriter_text($cc,$numero_total_solicitudes);
                xmlwriter_end_element($cc);  
                // Monto Total de Solicitudes
                xmlwriter_start_element($cc, 'c6');
                xmlwriter_text($cc,$monto_total_solicitudes);
                xmlwriter_end_element($cc);   
                // Numero Total de Confirmaciones
                xmlwriter_start_element($cc, 'c7');
                xmlwriter_text($cc,$numero_total_confirmaciones);
                xmlwriter_end_element($cc);                           
                // Monto Total de Confirmaciones
                xmlwriter_start_element($cc, 'c8');
                xmlwriter_text($cc,$monto_total_confirmaciones);
                xmlwriter_end_element($cc);                  
                // Identificador del Proveedor Tecnologico
                xmlwriter_start_element($cc, 'c9');
                xmlwriter_text($cc, $EUR);
                xmlwriter_end_element($cc); 

                xmlwriter_end_element($cc); 

                xmlwriter_start_element($cc, 'llaveAES');
                xmlwriter_text($cc, 0);
                xmlwriter_end_element($cc); 

                
                xmlwriter_end_element($cc); 

                xmlwriter_end_document($cc);
                
                $ccfile = xmlwriter_output_memory($cc);

                $ccname = "LR_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . ".CC";


                $path = "/var/www/apps/Smartbus/htarocc_v1.3/upload/" . $ccname;

                file_put_contents($path ,$ccfile);

              }
          }

      //    $this->generar_recargasremotas_xml($recargasremotas,$id_colecta,$fecha_colecta,$EUR,$id_ruta);

        //  $this->generar_cifras_control($recargasremotas,$id_colecta,$fecha_colecta,$fecha_inicial,$fecha_final,$EUR,$id_ruta,$id_bpd,$proveedor_tecnologico);
      
        //  Bitacora::infoLog($fechacorte); 

    }


/*

    function generar_recargasremotas_xml($transacciones,$id_colecta,$fecha_colecta,$EUR,$id_ruta)
     {

        $xw = xmlwriter_open_memory();
        xmlwriter_set_indent($xw, 1);
        $res = xmlwriter_set_indent_string($xw, ' ');
        xmlwriter_start_document($xw, '1.0', 'UTF-8');

        xmlwriter_start_element($xw, 'archivo');

        xmlwriter_start_attribute($xw, 'tipo');
        xmlwriter_text($xw, 'lapr');
        xmlwriter_end_attribute($xw);

        xmlwriter_start_element($xw, 'solicitudes');

        foreach ($transacciones as $transaccion) {

            xmlwriter_start_element($xw, 'recarga');
          
            // Identificador de tarjeta
            xmlwriter_start_element($xw, 'c1');
            xmlwriter_text($xw, $transaccion['payload']['uid']);
            xmlwriter_end_element($xw); 

            // Fecha y hora de registro
            xmlwriter_start_element($xw, 'c2');
            xmlwriter_text($xw, $transaccion['payload']['fecha_accion_registro']);
            xmlwriter_end_element($xw); 

            // Numero de Accion Aplicada al producto
            xmlwriter_start_element($xw, 'c3');
            xmlwriter_text($xw, $transaccion['payload']['numeroAccionAplicada']);
            xmlwriter_end_element($xw); 

            // Monto de la recarga
            xmlwriter_start_element($xw, 'c4');
           // xmlwriter_text($xw,number_format((float)$transaccion['payload']['monto'], 2, '.', '') );
            xmlwriter_end_element($xw); 

            // Producto
            xmlwriter_start_element($xw, 'c5');
            xmlwriter_text($xw, $transaccion['payload']['idProducto'] );
            xmlwriter_end_element($xw); 

            // Tipo de recarga 
            xmlwriter_start_element($xw, 'c6');
            // xmlwriter_text($xw, $transaccion['payload']['idProducto'] );
            xmlwriter_end_element($xw); 

             // Identificador de SAM
             xmlwriter_start_element($xw, 'c7');
             xmlwriter_text($xw, $transaccion['payload']['sam'] );
             xmlwriter_end_element($xw); 

           


        }
        xmlwriter_end_document($xw);
        xmlwriter_end_document($xw);


        $xmlfile = xmlwriter_output_memory($xw);

        $xmlname = "LR_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . "_" . $id_ruta;

        $path = "/var/www/apps/Smartbus/storage/" . $xmlname . ".DAT";

        file_put_contents($path ,$xmlfile);
    
     }
*/


   /* function generar_cifras_control($transacciones,$id_colecta,$fecha_colecta,$fecha_inicial,$fecha_final,$EUR,$id_ruta,$id_bpd,$proveedor_tecnologico){


      $monto_total = 0;
      $numero_total = 0;
      $viajes_bpd = 0;

      foreach ($transacciones as $transaccion) {

            if($transaccion['payload']['encabezado']['id_producto'] == $id_bpd)
            {

              $viajes_bpd++;
            }

        
            $monto_total += $transaccion['payload']['encabezado']['monto'];
            $numero_total++;
        }





      $xw = xmlwriter_open_memory();
      xmlwriter_set_indent($xw, 1);
      $res = xmlwriter_set_indent_string($xw, ' ');
    
      xmlwriter_start_element($xw, 'archivo');

      xmlwriter_start_attribute($xw, 'tipo');
      xmlwriter_text($xw, 'recarga');
      xmlwriter_end_attribute($xw);

      xmlwriter_start_element($xw, 'versionlayout');
      xmlwriter_text($xw, '1.0');
      xmlwriter_end_element($xw);

      xmlwriter_start_element($xw, 'cifrascontrol');

      // Identificador de colecta
      xmlwriter_start_element($xw, 'c1');
      xmlwriter_text($xw, $id_colecta);
      xmlwriter_end_element($xw); 

      // Fecha y Hora de colecta
      xmlwriter_start_element($xw, 'c2');
      xmlwriter_text($xw,$fecha_colecta);
      xmlwriter_end_element($xw); 

      // Fecha y hora inicial de corte
      xmlwriter_start_element($xw, 'c3');
      xmlwriter_text($xw,$fecha_inicial);
      xmlwriter_end_element($xw); 

      // Fecha y hora final de corte
      xmlwriter_start_element($xw, 'c4');
      xmlwriter_text($xw, $fecha_final);
      xmlwriter_end_element($xw); 

      // Identificador del operador
      xmlwriter_start_element($xw, 'c5');
      xmlwriter_text($xw, $EUR);
      xmlwriter_end_element($xw); 

      //identificador del corredor/ruta
      xmlwriter_start_element($xw, 'c6');
      xmlwriter_text($xw, $id_ruta);
      xmlwriter_end_element($xw); 

      // Numero total de registros
      xmlwriter_start_element($xw, 'c7');
      xmlwriter_text($xw, $numero_total);
      xmlwriter_end_element($xw);

      // Monto total de registros
      xmlwriter_start_element($xw, 'c8');
     // xmlwriter_text($xw, number_format((float)$monto_total 2, '.', ''));
      xmlwriter_end_element($xw);

      // Identificador del proveedor tecnologico 
      xmlwriter_start_element($xw, 'c9');
      xmlwriter_text($xw ,$proveedor_tecnologico);
      xmlwriter_end_element($xw);

      // Total de viajes(BPD's)
      xmlwriter_start_element($xw, 'c10');
      xmlwriter_text($xw, $viajes_bpd);
      xmlwriter_end_element($xw);

      xmlwriter_end_element($xw);

      xmlwriter_end_document($xw);

      $xmlfile = xmlwriter_output_memory($xw);

      $xmlname = "R_" . sprintf('%010d',$id_colecta) . "_" . $fecha_colecta . "_" . $EUR . "_" . $id_ruta;

      $path = "/var/www/apps/Smartbus/storage/" . $xmlname . ".CC";
      
      file_put_contents($path ,$xmlfile);

     }*/
}


