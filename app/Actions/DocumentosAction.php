<?php

namespace Smartbus\Actions;

use Klay\Actions\DocumentosAction as DocumentosActionBase;
use Smartbus\Console\Commands\DebitoXML;
use Klay\Models\Catalogo;
use Klay\Models\Documento;
use Smartbus\Events\UpdateLocationEvent;
use Smartbus\Library\Bitacora;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DocumentosAction extends DocumentosActionBase
{

    public function fimpeuploads()
    {

        $dir = storage_path();

        $archivos = File::files($dir);

        $files = [];

        foreach($archivos as $archivo){

           // $fileExtensions[$archivo->getExtension()] = $archivo->getExtension();

                if($archivo->getExtension() == 'DAT' or $archivo->getExtension() == 'CC'){

                    $xml = simplexml_load_file($archivo);

                    $files[$archivo->getFileName()] =  $xml->asXML();




                }



        }

        return [
            'status' => 'success',
            'data'=> $files
        ];

    }

    public function fimpeDownloads()
    {

        $dir = storage_path();

        $archivos = File::files($dir);

        $files = [];

        foreach($archivos as $archivo){

           // $fileExtensions[$archivo->getExtension()] = $archivo->getExtension();

                if($archivo->getExtension() == 'DAT' or $archivo->getExtension() == 'CC'){

                    $xml = simplexml_load_file($archivo);

                    $files[$archivo->getFileName()] =  $xml->asXML();




                }



        }

        return [
            'status' => 'success',
            'data'=> $files
        ];

    }


    public function fromfimpe()
    {
      /*  $request = request()->all();
       // Bitacora::infoLog($request);

       $registros_fimpe = null;

       if(array_key_exists('fecha', $request))
       {
         $registros_fimpe = (new Documento('registros_fimpe'))->where('payload->fecha', $request['fecha'])->all();
       }

       if(array_key_exists('ruta', $request))
       {
        $registros_fimpe = (new Documento('registros_fimpe'))->where('payload->ruta', $request['ruta'])->all();
       }

       if(array_key_exists('fecha', $request)  and array_key_exists('ruta', $request))
       {
        $registros_fimpe = (new Documento('registros_fimpe'))->where('payload->ruta', $request['ruta'])->where('payload->ruta', $request['fecha'])->all();
       }

       //$registro_fimpe = $registros_fimpe->first();

     


      foreach ($registros_fimpe as $registro_fimpe) {

        //Bitacora::infoLog(  $registro_fimpe);

        //$registro_fimpe['payload']['fecha'] = '2023-07-16';


            if($registro_fimpe['payload']['tipo'] == 'Debito')
            {

              $validaciones =   (new Documento('validaciones'))->where('payload->encabezado->fecha_hora','>=','2023-09-05 00:00:00')->where('payload->encabezado->fecha_hora','<=','2023-09-06 23:59:59')->all();

              $validaciones = json_decode($validaciones,true);

              $xmlfile = new DebitoXML::generar_debito_xml($validaciones);
              
             

             return [
                'status' => 'success',
                'data'=>  $validaciones
            ];

            }
       }

       
      
*/
   


    }

    /**
     * Funcion para guardar el tracking que envia la alcancia
    */
    public function tracking()
    {
        // Obtenemos los valores de la peticion
        $request = request()->all();
        Bitacora::infoLog($request);
        
        // Buscamos que el vehiculo exista
        $vehiculo = (new Catalogo('vehiculos'))->findorFail($request['payload']['id_vehiculo']);

        // Registramos la hora a la que se envio el dato
        $request['payload']['hora'] = date('H:i:s');

        // Registramos el usuario que hizo el registro
        $autor['id'] = auth()->user()->id;
        $autor['correo'] = auth()->user()->nombre;
        $autor['scope'] = kw2p_ambito();
        
        // Buscamos si ya existe un documento con la fecha de hoy del vehiculo
        if($documento =  (new Documento('tracking'))->where('payload->encabezado->id_vehiculo', $request['payload']['id_vehiculo'])->where('payload->encabezado->fecha', date('Y-m-d'))->first())
        {
            // Si existe solo actualizamos la hora y y la ubicacion, y almacenamos el punto en recorrido
            $items = $documento->payload['recorrido'];
            
            $payload = [
                'encabezado' => [
                    'fecha' => date('Y-m-d'),
                    'actualizacion' => $request['payload']['hora']
                ]
            ];

            if ($request['payload']['latitud'] || $request['payload']['longitud'])
            {
                $payload['encabezado']['posicion_actual'] = [
                    'latitud' => $request['payload']['latitud'],
                    'longitud' => $request['payload']['longitud']
                ];

                array_push($items, $request['payload']);

                $payload['recorrido'] = $items;
            }
            
            
            $response = $documento->actualizar($payload, $autor);

        }
        else
        {
            // Si no existe creamos todo el documento
            $payload = [
                'encabezado' =>[
                    'id_vehiculo' => $request['payload']['id_vehiculo'],
                    'fecha' => date('Y-m-d'),
                    'actualizacion'=> $request['payload']['hora']
                ]
            ];
            if ($request['payload']['latitud'] || $request['payload']['longitud'])
            {
                $payload['encabezado']['posicion_actual'] = [
                    'latitud' => $request['payload']['latitud'],
                    'longitud' => $request['payload']['longitud']
                ];

                $payload['recorrido'] = [$request['payload']];
            }
            else
            {
                $payload['recorrido'] = [];
            }
            
            $response = (new Documento('tracking'))->crear('',$payload, $autor);
        }
        // Retornamos success si todo sale bien
       // event(new UpdateLocationEvent($response, $vehiculo));
        return [
            'status' => 'success',
            'data'=> $response
        ];
    }

    /**
     * Funcion para obtener la ultima ubicacion de todas las unidades
     */
    function ubicacion() {
        
        // Obtenermos todos los vehiculos
        $vehiculos = (new Catalogo('vehiculos'))->all();
        $vehiculos = json_decode($vehiculos, true);
        $result = [];

        // Recorremos el arreglo $vehiculos
        foreach($vehiculos as $vehiculo){
            // Si existe un archivo de ubicacion de la unidad, se toma la ubicacion de ahi
            if ($ubicacion = (new Documento('tracking'))->orderBy('payload->encabezado->fecha')->where('payload->encabezado->id_vehiculo', $vehiculo['id'])->first()){
                $result[] = [
                    'id_vehiculo' => $vehiculo['id'],
                    'label' => $vehiculo['payload']['label'],
                    'fecha' => $ubicacion['payload']['encabezado']['fecha'],
                    'posicion_actual' =>  $vehiculo['payload']['location']
                    
                ];
            }
            // En caso contrario lo ubicacion en 0,0
            else {
                $result[] = [
                    'id_vehiculo' => $vehiculo['id'],
                    'label' => $vehiculo['payload']['label'],
                    'fecha' => '0000-00-00',
                    'posicion_actual' => ['latitud' => 0, 'longitud' => 0],
                    'actualizacion' => '00:00:00'
                ];
            }
        }
        return [
            'status' => 'success',
            'data' => $result
        ];
    }
}
