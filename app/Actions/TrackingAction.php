<?php
namespace Smartbus\Actions;

use Klay\Actions\ResolutionAction;
use Klay\Models\Catalogo;
use Klay\Models\Documento;

use Smartbus\Library\Bitacora;

class TrackingAction extends ResolutionAction
{
    

    public function test()
    {
        
        Bitacora::infoLog("Se ingreso en test");
    }

    function create()
    {
         // Obtenemos los valores de la peticion
         $request = request()->all();
         Bitacora::infoLog($request);
         $createResponse = null;
         $response = null;
         
         // Buscamos que el vehiculo exista
         $vehiculo = (new Catalogo('vehiculos'))->findorFail($request['payload']['id_vehiculo']);
         $VehiculoSpeed = null;
         $VehiculoKilometros = null;
         
         // Registramos la hora a la que se envio el dato
        // $request['payload']['hora'] = date('H:i:s');
 
         // Registramos el usuario que hizo el registro
         $autor['id'] = auth()->user()->id;
         $autor['correo'] = auth()->user()->nombre;
         $autor['scope'] = kw2p_ambito();
         
         // Buscamos si ya existe un documento con la fecha de hoy del vehiculo
         if($documento =  (new Documento('tracking'))->where('payload->encabezado->id_vehiculo', $request['payload']['id_vehiculo'])->where('payload->encabezado->fecha', $request['payload']['fecha'])->first())
         {
             
 
             if ($request['payload']['latitud'] || $request['payload']['longitud'])
             {
                // Si existe solo actualizamos la hora y y la ubicacion, y almacenamos el punto en recorrido
                $items = $documento->payload['recorrido'];

                $last_item = end($items);
                $last_latitud = $last_item['latitud'];
                $last_longitud = $last_item['longitud'];

                $first_item = $items[0];
                $first_latitud = $first_item['latitud'];
                $first_longitud = $first_item['longitud'];
             
                $payload = [
                 'encabezado' => [
                     'fecha' =>  $request['payload']['fecha'],
                     'id_vehiculo' =>  $request['payload']['id_vehiculo']                  
                     ]
                ];

                //Calculate distance from last
                $earthRadius = 6371000;
                $latFrom = deg2rad($last_latitud);
                $lonFrom = deg2rad($last_longitud);
                $latTo = deg2rad($request['payload']['latitud']);
                $lonTo = deg2rad($request['payload']['longitud']);
              
                $latDelta = $latTo - $latFrom;
                $lonDelta = $lonTo - $lonFrom;
              
                $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
                
                $distance = $angle * $earthRadius;


                $lastTime = strtotime($last_item['hora']);
                $currentTime = strtotime($request['payload']['hora']);
                    
                $time = $currentTime - $lastTime;


                $VehiculoSpeed = (3600/$time) * ($distance/1000);
                $VehiculoSpeed = round($VehiculoSpeed, 2);


                $VehiculoKilometros = $last_item['kilometros'] + ($distance/1000);
                $VehiculoKilometros = round($VehiculoKilometros, 2);



                
                //InfoWindow Vehicle 
                if($vehiculo != null)
                {
                    if(!($documento =  (new Documento('tracking'))->where('payload->encabezado->id_vehiculo', $request['payload']['id_vehiculo'])->where('payload->encabezado->fecha','>', $request['payload']['fecha'])->first()))
                    {            
                    $vehiculo['payload'] = [

                        'label' =>  $vehiculo['payload']['label'],
                        'active' => true,
                        'dispositivo' => $vehiculo['payload']['dispositivo'],
                        'external_id' => $vehiculo['payload']['external_id'],
                        'id_ruta' => $request['payload']['id_ruta'],
                        'operador'=> $request['payload']['id_operador'],
                        'location' => [
                            'fecha_hora_servidor' =>  date("Y-m-d H:i:s"),
                            'fecha_hora' => $request['payload']['fecha']  . " " . $request['payload']['hora'],
                            'latitud'   => $request['payload']['latitud'],
                            'longitud'  => $request['payload']['longitud'],
                            'subidas' => $request['payload']['subidas'],
                            'bajadas' => $request['payload']['bajadas'],
                            'velocidad' => $VehiculoSpeed, 
                            'kilometros'  => $VehiculoKilometros ,    
                            'kilometros_acumulados'  => $vehiculo['payload']['location']['kilometros_acumulados'] + $VehiculoKilometros 
                             ]
                    ];
                        $createResponse = $vehiculo->actualizar($vehiculo['payload']);
                    }
                }



               


                if($distance > 30)
                {
                    //Calculate speed

                
                          

                 array_push($items,[
                    'id_operador'=> $request['payload']['id_operador'],
                    'id_ruta'=> $request['payload']['id_ruta'],
                    'hora' => $request['payload']['hora'],
                    'latitud' => $request['payload']['latitud'],
                    'longitud' => $request['payload']['longitud'],
                    'velocidad' => $VehiculoSpeed,
                    'kilometros'  => $VehiculoKilometros,
                    'kilometros_acumulados' =>  $vehiculo['payload']['location']['kilometros_acumulados'] + $VehiculoKilometros ,
                    'subidas' => $request['payload']['subidas'],
                    'bajadas' => $request['payload']['bajadas'],
                    ]);
 
                 $payload['recorrido'] = $items;

                 $response = $documento->actualizar($payload, $autor);
                }

                else {
                    {
                        return [
                            'status' => 'success',
                            'data'=> $createResponse
                        ];
                    }
                }
             }
             
             
            
 
         }
         else
         {
             // Si no existe creamos todo el documento

              //InfoWindow Vehicle 
              if($vehiculo != null)
              {
                  if(!($documento =  (new Documento('tracking'))->where('payload->encabezado->id_vehiculo', $request['payload']['id_vehiculo'])->where('payload->encabezado->fecha','>', $request['payload']['fecha'])->first()))
                  {            
                  $vehiculo['payload'] = [

                      'label' =>  $vehiculo['payload']['label'],
                      'active' => true,
                      'dispositivo' => $vehiculo['payload']['dispositivo'],
                      'external_id' => $vehiculo['payload']['external_id'],
                      'id_ruta' => $request['payload']['id_ruta'],
                      'operador'=> $request['payload']['id_operador'],
                      'location' => [
                          'fecha_hora_servidor' =>  date("Y-m-d H:i:s"),
                          'fecha_hora' => $request['payload']['fecha']  . " " . $request['payload']['hora'],
                          'latitud'   => $request['payload']['latitud'],
                          'longitud'  => $request['payload']['longitud'],
                          'subidas' => $request['payload']['subidas'],
                          'bajadas' => $request['payload']['bajadas'],
                          'velocidad' => 0, 
                          'kilometros'  => 0,
                          'kilometros_acumulados'=> 0            
                          ]
                  ];
                      $createResponse = $vehiculo->actualizar($vehiculo['payload']);
                  }
              }

           
             if ($request['payload']['latitud'] || $request['payload']['longitud'])
             {

                $payload = [
                    'encabezado' =>[

                        'id_vehiculo' => $request['payload']['id_vehiculo'],
                        'fecha' => $request['payload']['fecha']
                    ],

                    'recorrido' => [
                        [
                        'id_operador'=> $request['payload']['id_operador'],
                        'id_ruta'=> $request['payload']['id_ruta'],
                        'hora' => $request['payload']['hora'],
                        'latitud' => $request['payload']['latitud'],
                        'longitud' => $request['payload']['longitud'],
                        'subidas' => $request['payload']['subidas'],
                        'bajadas' => $request['payload']['bajadas'],
                        'kilometros' => 0,
                        'kilometros_acumulados'=> 0     
                        ]
                    ]
                ];


                 $response = (new Documento('tracking'))->crear('',$payload, $autor);
             }
           
             
            
         }
         // Retornamos success si todo sale bien


         return [
             'status' => 'success',
             'data'=> $response
         ];
    } 

    

    function latest() {
        
        // Obtenermos todos los vehiculos
        $vehiculos = (new Catalogo('vehiculos'))->all();
        $vehiculos = json_decode($vehiculos, true);
        $result = [];

        // Recorremos el arreglo $vehiculos
        foreach($vehiculos as $vehiculo){

            // Si existe un archivo de ubicacion de la unidad, se toma la ubicacion de ahi
           //$ubicacion = (new Documento('tracking'))->where('payload->encabezado->id_vehiculo', $vehiculo['id'])->all();
           $result[] = $vehiculos;
           /*
           if  ($ubicacion = (new Documento('tracking'))->orderBy('payload->encabezado->fecha')->where('payload->encabezado->id_vehiculo', $vehiculo['id'])){
               $result[] = [
                    'id_vehiculo' => $vehiculo['id'],
                    'label' => $vehiculo['payload']['label'],
                    'fecha' => $ubicacion['payload']['encabezado']['fecha'],
                    'posicion_actual' => $ubicacion['payload']['encabezado']['posicion_actual'],
                    'actualizacion' => $ubicacion['payload']['encabezado']['actualizacion']
                ];

                $result[] = $ubicacion;
              
            }*/
            // En caso contrario lo ubicacion en 0,0
           /* else {
                $result[] = [
                    'id_vehiculo' => $vehiculo['id'],
                    'label' => $vehiculo['payload']['label'],
                    'fecha' => '0000-00-00',
                    'posicion_actual' => ['latitud' => 0, 'longitud' => 0],
                    'actualizacion' => '00:00:00'
                ];
            }*/
        }
        return [
            'status' => 'success',
            'data' => $result
        ];
    }
}
