<?php

namespace Smartbus\Actions;

use Klay\Actions\ResolutionAction;
use Klay\Models\Catalogo;
use Klayware\Exceptions\KlayException;
use Smartbus\Library\Bitacora;

class OperadoresAction extends ResolutionAction
{
    public function actualizarBefore(&$data, $catalogo)
    {
        Bitacora::infoLog($data);
        //Se valida que el external_id no este repetido
        if($registro = (new Catalogo("operadores"))->where('payload->external_id', $data['payload']['external_id'])->first()){
            if ( $registro->id !== ($data['id'] ?? null )){
                throw (new  KlayException("Propiedad external_id ya existe y no se puede repetir", "external_id repetido"))->status(400);
            }
        }
    }
}
