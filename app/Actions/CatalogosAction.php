<?php

namespace Smartbus\Actions;

use Klay\Actions\CatalogosAction as CatalogosActionBase;
use Klay\Models\Catalogo;
use Klayware\Exceptions\KlayException;
use Smartbus\Library\Bitacora;

class CatalogosAction extends CatalogosActionBase
{
    /**
     * Funcion para obtener un resumen de los datos para el dashboard
     */
     public function dashboard()
    {
        $vehiculos = (new Catalogo('vehiculos'))->all()->count();
        $dispositivos = (new Catalogo('dispositivos'))->all()->count();
        $operadores = (new Catalogo('operadores'))->all()->count();
        $rutas = (new Catalogo('rutas'))->all()->count();
        return [
            'status' => 'success',
            'data' => [
                'unidades'=> $vehiculos,
                'dispositivos' => $dispositivos,
                'operadores' => $operadores,
                'rutas' => $rutas
            ]
        ];
    }
    /**
     * Funcion para obtener la identidad de un vehiculo
     */
    public function identidad()
    {
        $request = request()->all();
        Bitacora::infoLog($request);
        
        if ($registro = (new Catalogo('dispositivos'))->where('payload->external_id', $request['external_id'])->first()){
            $registroPaayload = json_decode($registro, true);

            if ($vehiculo = (new Catalogo('vehiculos'))->where('payload->dispositivo', $registroPaayload['id'])->first()){


          

                $vehiculoPayload = json_decode($vehiculo, true);

                $ruta = (new Catalogo('rutas'))->where('payload->external_id',sprintf('%04d', $vehiculoPayload['payload']['id_ruta']))->first();
                
                    //Bitacora::infoLog("ruta: ");
                    
                    $rutaPayload = json_decode($ruta, true);
                    Bitacora::infoLog( $rutaPayload);
                
                
                //Bitacora::infoLog(sprintf('%04d', $vehiculoPayload['payload']['id_ruta']));
               


                $response = [
                    'status' => 'success',
                    'data' => [
                        'entidad' => '1900',
                        'idUnidad' => $vehiculoPayload['payload']['external_id'],
                        'idDispositivoDB' => $registro->id,
                        'idDispositivo' => $registroPaayload['payload']['external_id'],
                        'tipoTransporte' => '01',
                        'transporte' => 'Camion',
                        'idTransporte' => $vehiculoPayload['payload']['label'],
                        'idVehiculo' => $vehiculo->id,
                        'idTipoDispositivo' => '0001',
                        'empresa' => 'Smartbus',
                        'idRuta' => (int)$rutaPayload['payload']['external_id'],
                        'rutaLabel' => $rutaPayload['payload']['label'],
                        'activo' => $vehiculoPayload['payload']['active'] ?? true 
                    ]
                ];
                Bitacora::infoLog($response);
                return $response;
            }
        }
    }
    
    /**
     * Funcion para obtener las reglas de operacion
     */
    public function operacion()
    {
        // Obtenemos catalogos de perfiles y productos
        $perfiles = (new Catalogo('perfiles'))->all();
        $productos = (new Catalogo('productos'))->all();
        
        // Validamos que existan. Si no existen, no se puede mandar informacion    
        if (!$perfiles || !$productos){
            throw (new KlayException('Valores de operacion invalidos', ['valores_incompletos']))->status(400);
        }

        $perfiles = json_decode($perfiles, true);
        $perfiles_alcancia = [];
        // Recorremos el arreglo de perfiles y formateamos para la alcancia
        foreach($perfiles as $perfil) {
            $perfiles_alcancia[] = [
                'codigoPerfil' => $perfil['payload']['codigo_perfil'],
                'usuarioTipo' => $perfil['payload']['usuario_tipo'],
                'antiPassback' => $perfil['payload']['anti_passback'],
                'credito' => $perfil['payload']['credito'],
                'transbordo' => $perfil['payload']['transbordo']
            ];
            foreach($perfil['payload']['productos'] as $prod){
                $perfiles_alcancia['productos'][$prod['productoId']] = [
                    'tarifa' => $prod['tarifa'],
                    'maximoValor' => $prod['maximoValor']
                ];
            }
        }

        //Recorremos el arreglo de productos y formateamos para la alcancia
        $productos_alcancia = [];
        $productos = json_decode($productos,true);
        foreach($productos as $product){
            $productos_alcancia[$product['payload']['id_producto']] = [
                'productoNombre' => $product['payload']['producto_nombre'],
                'productoClave' => $product['payload']['clave'],
                'recarga' => $product['payload']['opciones']['recarga'],
                'validacion' => $product['payload']['opciones']['validacion'],
                'contrato' => $product['payload']['opciones']['contrato'],
                'servicio' => $product['payload']['opciones']['servicio']
            ];
        }

        // Obtenemos la matriz de transbordos y la formatemaos para la alcancia
        $transbordos = (new Catalogo('transbordos'))->all();
        $transbordos_alcancia = [];
        if ($transbordos){
            $transbordos = json_decode($transbordos, true);
            Bitacora::infoLog($transbordos);
            foreach($transbordos as $entidad){
                $transbordos_alcancia[$entidad['payload']['external_id']] = [];
                foreach($entidad['payload']['transbordos'] as $trans){
                    $transbordos_alcancia[$entidad['payload']['external_id'][$trans['id_ruta']]] = [
                        'tiempo' => $trans['tiempo'],
                        'tarifa' => $trans['tarifa']
                    ];
                }
            }
        }

        // Enviamos los datos
        Bitacora::infoLog($transbordos_alcancia);
        return [
            'productos' => $productos_alcancia,
            'perfiles' => $perfiles_alcancia,
            'matrizTransbordos' => $transbordos_alcancia
        ];
    }
}
