<?php

namespace Smartbus\Actions;

use Klay\Actions\ResolutionAction;
use Smartbus\Library\Bitacora;
use Klay\Models\Catalogo;
use Klayware\Exceptions\KlayException;

class VehiculosAction extends ResolutionAction
{
    public function actualizarBefore(&$data, $catalogo)
    {
        //Si se envia relacion con dispositivo
        if (isset($data['payload']['id_dispositivo'])){
            //Se validan que el dispositivo exista
            (new Catalogo("dispositivos"))->findOrFail($data['payload']['id_dispositivo']);
            
            //Se valida que el dispositivo no este asignado a otra unidad
            if((new Catalogo("vehiculos"))->where('payload->id_dispositivo', $data['payload']['id_dispositivo'])->first()){
                throw (new  KlayException("El dispositivo ya esta asignado a otra unidad", "id_dispositivo asignado"))->status(400);
            }
        }

        //Si se envia relacion con el operador
        if (isset($data['payload']['id_operador'])){
            //Se validan que el operador exista
            (new Catalogo("operadores"))->findOrFail($data['payload']['id_operador'] ?? null);
            
            //Se valida que el operador no este asignado a otra unidad
            if((new Catalogo("vehiculos"))->where('payload->id_operador', $data['payload']['id_operador'])->first()){
                throw (new  KlayException("El operador ya esta asignado a otra unidad", "id_operador asignado"))->status(400);
            }
        }
        
        //Si se envia relacion con la ruta
        if(isset($data['payload']['id_ruta'])){
            //Se validan que la ruta exista
            (new Catalogo("rutas"))->findOrFail($data['payload']['id_ruta'] ?? null);
        }

        //Se valida que el external_id no este repetido
        if($registro = (new Catalogo("vehiculos"))->where('payload->external_id', $data['payload']['external_id'] ?? null)->first()){
            if ( $registro->id !== ($data['id'] ?? null )){
                throw (new  KlayException("Propiedad external_id ya existe y no se puede repetir", "external_id repetido"))->status(400);
            }
        }

        
    }
}
