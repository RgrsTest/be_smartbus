<?php
namespace Smartbus\Actions;

use Klay\Actions\ResolutionAction;
use Klay\Models\Catalogo;
use Klay\Models\Documento;

use Smartbus\Library\Bitacora;

class RecargasAction extends ResolutionAction
{
    public function crearBefore(&$data, $documento)
    {
        Bitacora::infoLog("Se ingreso una validacion");
        Bitacora::infoLog($data);

          //Validamos Unidad
          (new Catalogo("vehiculos"))->findOrFail($data['payload']['encabezado']['id_vehiculo']);

          //Validamos dspositivo
          (new Catalogo("dispositivos"))->findOrFail($data['payload']['encabezado']['id_dispositivo']);
  
          //Validamos ruta
          (new Catalogo("rutas"))->findOrFail($data['payload']['encabezado']['id_ruta']);
  
          //Validamos operador
          (new Catalogo("operadores"))->findOrFail($data['payload']['encabezado']['id_operador']);
  
          //Formateamos los datos de acuerdo a las reglas del schema
          $data['payload']['encabezado']['id_vehiculo'] = intval($data['payload']['encabezado']['id_vehiculo']);
          $data['payload']['encabezado']['id_dispositivo'] = intval($data['payload']['encabezado']['id_dispositivo']);
          $data['payload']['encabezado']['id_ruta'] = intval($data['payload']['encabezado']['id_ruta']);
          $data['payload']['encabezado']['id_operador'] = intval($data['payload']['encabezado']['id_operador']);
  
          $data['payload']['encabezado']['monto'] =  intval($data['payload']['encabezado']['monto'])/100;
          $data['payload']['encabezado']['saldo_inicial'] =  intval($data['payload']['encabezado']['saldo_inicial'])/100;
          $data['payload']['encabezado']['saldo_final'] =  intval($data['payload']['encabezado']['saldo_final'])/100;
          
  
         
          
          $data['payload']['encabezado']['consecutivo_aplicacion'] =  intval($data['payload']['encabezado']['consecutivo_aplicacion']);
          
          $data['payload']['encabezado']['latitud'] = floatval($data['payload']['encabezado']['latitud']);
          $data['payload']['encabezado']['longitud'] = floatval($data['payload']['encabezado']['longitud']);
  

        $data['payload']['encabezado']['fecha_hora_servidor'] =  date("Y-m-d H:i:s");
    }

    public function crear($data)
    {
        //Validamos que no se repita el documento
        if ($validacion = (new Documento('recargas'))->where('payload->encabezado->folio_transaccion', $data['payload']['encabezado']['folio_transaccion'])->first())
        {
            Bitacora::infoLog('Se encontro una venta con el mismo folio de transaccion');
            return response()->json(
                [
                    'status' => 'success',
                    'data' => $validacion
                ]);
        }
        $autor['id'] = auth()->user()->id;
        $autor['correo'] = auth()->user()->nombre;
        $autor['scope'] = kw2p_ambito();
        
        $validacion = (new Documento('recargas'))->crear('', $data['payload'], $autor);
        return response()->json(
            [
                'status' => 'success',
                'data' => $validacion
            ]);
    }
}