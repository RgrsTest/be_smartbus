<?php
namespace Smartbus\Actions;

use Klay\Actions\ResolutionAction;
use Klay\Models\Catalogo;
use Klay\Models\Documento;

use Smartbus\Library\Bitacora;

class VentasAction extends ResolutionAction
{
    public function crearBefore(&$data, $documento)
    {
        Bitacora::infoLog("Se ingreso una venta");
        Bitacora::infoLog($data);
  //Validamos Unidad
  (new Catalogo("vehiculos"))->findOrFail($data['payload']['encabezado']['id_vehiculo']);

  //Validamos dspositivo
  (new Catalogo("dispositivos"))->findOrFail($data['payload']['encabezado']['id_dispositivo']);

  //Validamos ruta
  (new Catalogo("rutas"))->findOrFail($data['payload']['encabezado']['id_ruta']);

  //Validamos operador
  (new Catalogo("operadores"))->findOrFail($data['payload']['encabezado']['id_operador']);

  //Formateamos datos para ser almacenados
  $data['payload']['encabezado']['id_vehiculo'] = intval($data['payload']['encabezado']['id_vehiculo']);
  $data['payload']['encabezado']['id_dispositivo'] = intval($data['payload']['encabezado']['id_dispositivo']);
  $data['payload']['encabezado']['id_ruta'] = intval($data['payload']['encabezado']['id_ruta']);
  $data['payload']['encabezado']['id_operador'] = intval($data['payload']['encabezado']['id_operador']);

  $data['payload']['encabezado']['monto'] =  intval($data['payload']['encabezado']['monto'])/100;

  
  
  $data['payload']['encabezado']['latitud'] = floatval($data['payload']['encabezado']['latitud']);
  $data['payload']['encabezado']['longitud'] = floatval($data['payload']['encabezado']['longitud']);

        
     

        $data['payload']['encabezado']['fecha_hora_servidor'] =  date("Y-m-d H:i:s");
    }

    public function crear($data)
    {
        //Validamos que no se repitan las ventas
        if ($venta = (new Documento('ventas'))->where('payload->encabezado->folio_transaccion', $data['payload']['encabezado']['folio_transaccion'])->first())
        {
            Bitacora::infoLog('Se encontro una venta con el mismo folio de transaccion');
            return response()->json(
                [
                    'status' => 'success',
                    'data' => $venta
                ]);
        }
        $autor['id'] = auth()->user()->id;
        $autor['correo'] = auth()->user()->nombre;
        $autor['scope'] = kw2p_ambito();
        
        $venta = (new Documento('ventas'))->crear('', $data['payload'], $autor);

        return response()->json(
            [
                'status' => 'success',
                'data' => $venta
            ]);

    }
}