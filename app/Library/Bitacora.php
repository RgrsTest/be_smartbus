<?php
namespace Smartbus\Library;

use Illuminate\Support\Facades\Log;

class Bitacora
{
  public static function infoLog($mensaje)
  {
    Log::channel("Smartbus")->info($mensaje);
  }

  public static function errorLog($mensaje)
  {
    Log::channel("Smartbus")->error($mensaje);
  }


}
