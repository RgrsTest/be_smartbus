<?php

require base_path('apps/Klay/database/migrations/2018_12_12_000001_klay_init.php');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmartbusInit extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    (new KlayInit)->up();

    # ...
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    (new KlayInit)->down();

    # ...
  }
}
